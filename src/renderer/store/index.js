import Vue from 'vue'
import Vuex from 'vuex'
import VueVideoPlayer from 'vue-video-player'
import 'video.js/dist/video-js.css'

import modules from './modules'

Vue.use(Vuex)
Vue.use(VueVideoPlayer)
export default new Vuex.Store({
  modules,
  strict: process.env.NODE_ENV !== 'production'
})
