import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/landing-page',
      name: 'landing-page',
      component: require('@/components/LandingPage').default
    },
    {
      path: '/',
      name: 'home',
      component: require('@/components/Home').default
    },
    {
      path: '/about',
      name: 'About',
      component: require('@/components/About/About').default
    },
    {
      path: '/calendar',
      name: 'Calendar',
      component: require('@/components/Calendar/Calendar').default
    },
    {
      path: '/video-page',
      name: 'VideoPage',
      component: require('@/components/VideoPage/VideoPage').default
    },
    {
      path: '/config',
      name: 'Config',
      component: require('@/components/Config/Config').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
